public class MotoUtil {

Moto moto1 = new Moto("Honda", "GoldWing", 120, 30000);
Moto moto2 = new Moto("Ducati", "Multistrada", 160, 17000);
Moto moto3 = new Moto("Yamaha", "Tmax", 55, 12000);
Moto moto4 = new Moto("Suzuki", "VStrom 650", 70, 8000);
Moto[] motos = { moto1, moto2, moto3, moto4};

public void informe() {
    double max= 0;
    double maxPotencia= 0;
    double precioTotal= 0;
    Moto guardaMoto = moto1;
    Moto guardaMotoPotencia = moto1;
    for (Moto numMoto : motos) {

        double comparador = numMoto.getPrecio();
        int comparadorPotencia = numMoto.getPotencia();
        precioTotal = precioTotal + comparador;

        if (comparador > max) {
           guardaMoto = numMoto;
           max = comparador; 
        }

        if (comparadorPotencia > maxPotencia) {
            guardaMotoPotencia = numMoto;
            maxPotencia = comparadorPotencia;
        }
        
    }

    System.out.println("La moto más cara es la "+guardaMoto.toString()+"("+guardaMoto.getPrecio()+")");
    System.out.println("La moto más potente es la"+guardaMotoPotencia.toString()+"("+guardaMotoPotencia.getPotencia()+")");
    System.out.println("El precio total es: "+precioTotal);
}


    public void compara(Moto m1, Moto m2) {
        
        Moto motoPotente = m1;
        Moto motoNoPotente = m2;
        Moto motoCara = m1;
        Moto motoNoCara = m2;

            if(m2.getPotencia()>m1.getPotencia()) {
                motoPotente = m2;
                motoNoPotente = m1;
            }

            if (m2.getPrecio()>m1.getPrecio()) {
                motoCara= m2;
                motoNoCara = m1;
            }

            System.out.println("La moto "+motoPotente+" es más potente que la "+motoNoPotente);
            System.out.println("La moto "+motoCara+" es más cara que la "+motoNoCara);
            
        
    }
}