import java.util.Arrays;
import java.util.List;
import java.util.HashSet;
import java.util.Set;

public class SetTest {
    

    public static void main(String[]args) {
        Set<Company> prueba = new HashSet<Company>();
        
        Company ob1 = new Company("Oracle", 1977);
        Company ob2 = new Company("IBM", 1911);
        Company ob3 = new Company("Oracle", 1977);
        Company ob4 = new Company("IBM", 1911);
        Company ob5 = new Company("IBM", 1965);

        prueba.add(ob1);
        prueba.add(ob2);
        prueba.add(ob3);
        prueba.add(ob4);
        prueba.add(ob5);
        prueba.add(ob1);

        System.out.println(prueba.size());


}

}