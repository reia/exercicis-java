package com.java_mireia.vehiculos;

class Coche extends Vehiculo {
    public int cilindrada;


    public Coche(int cm3) {

        // Llamamos al constructor de la clase superior con super. El 4 sería el número de ruedas.
        super(4); 
        this.cilindrada = cm3;
    }
}