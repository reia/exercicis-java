package com.java_mireia.vehiculos;


class TestVehiculo {

    public static void main(String[] args) {

        // Creamos un coche. El 1000 son los cm3 que es el atributo que nos pide el constructor de coche.
        Vehiculo v = new Coche(1000);

        v.numruedas = 4;
        Coche c = (Coche) v;
        c.cilindrada =1200;

        System.out.println(c.numruedas);
        System.out.println(c.cilindrada);


    }


                // HERENCIAS:

        // class Vehiculo {...}
        // class Coche extends Vehiculo {...}
        // class F1 extends Coche {...}
        // class Taxi extends Coche {...}

        // Vehiculo a = new Coche(); 
        
        // Se podrían asignar vehículos a coches, pero no al revés (de una clase más pequeña a una más grande.). Si necesitaramos que un taxi se declarara como Vehiculo pero tuviera los atributos de un taxi se puede castear:

        // Vehiculo a = new Taxi(); Taxi t = (Taxi)a;

        // No se puede castear a algo que no es. (Por ejemplo intentar castear un Formula1 si el objeto es taxi).
    
}