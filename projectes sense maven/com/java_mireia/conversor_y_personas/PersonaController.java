package com.java_mireia.conversor_y_personas;

public class PersonaController{



    public void muestraPersona(Persona pers){
        String nombre= pers.getNombre();
        int edad= pers.getEdad();
        String email= pers.getEmail();

        System.out.println(nombre+" tiene "+edad+" y su email es "+email);

    }

    public void comparaPersona (Persona pers1, Persona pers2) {
    
        int edad1= pers1.getEdad();
        int edad2= pers2.getEdad();
        String nombre1= pers1.getNombre();
        String nombre2= pers2.getNombre();
        

        if (edad1>edad2) {

            System.out.println(nombre1+ " es mayor que " +nombre2);
        } else {

            System.out.println(nombre2+ " es mayor que " +nombre1);
        }

 
        
    }
}