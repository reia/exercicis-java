package com.java_mireia.conversor_y_personas;


public class Conversor{

// Después de public, se pone int ya que la función va a devolver un dato int, por lo tanto se lo tenemos que decir. km2m: el nombre de la función. (int km): parámetro que le pasamos a la función. 

    // public double mih2kmh(double mih){
    //     return mih*1.6;
    // }

    public double convierte(double valor, String desde, String hacia) {

        double resultado= valor;

        if(desde == "kmh" && hacia == "mih") {
            resultado = valor*1.6;
        } else if (desde == "mih" && hacia == "kmh") {
            resultado = valor/1.6;
        }

        if(desde == "kmh" && hacia == "ydh") {
            resultado = valor*1093.6;
        } else if (desde == "ydh" && hacia == "kmh") {
            resultado = valor/1093.6;
        }

        if(desde == "ms" && hacia == "kmh") {
            resultado = valor*3.6;
        } else if (desde == "kmh" && hacia == "ms") {
            resultado = valor/3.6;
        }

        if(desde == "ms" && hacia == "mih") {
            resultado = valor*2.237;
        } else if (desde == "mih" && hacia == "ms") {
            resultado = valor/2.237;
        }

        if(desde == "ms" && hacia == "ydh") {
            resultado = valor*1.0936;
        } else if (desde == "ydh" && hacia == "ms") {
            resultado = valor/1760;
        }

        if(desde == "mih" && hacia == "ydh") {
            resultado = valor*1.0936;
        } else if (desde == "ydh" && hacia == "mig") {
            resultado = valor/1760;
        }

        if (desde == hacia) {
            resultado = valor;
        }



       return resultado;
    }


    }


    

    // public int km2m(int km){
    //     int respuesta = km*1000;
    //     return respuesta;
    // }

    // public void dobla(int a){
    //     a = a * 2;
    //     System.out.println("el doble es"+a);
    // }
