package com.java_mireia.conversor_y_personas;

class Test{
    public static void main(String[] args) {
        System.out.println("Hola");
        double numero = 20;
        String desde= "kmh";
        String hacia="ms";
        Conversor conversor1= new Conversor();
        double convertido = conversor1.convierte(numero, desde, hacia);
        
        System.out.printf(
            "%.2f %s equivalen a %.2f %s \n" ,
            numero,
            desde,
            convertido,
            hacia
        );


        Persona pers1 = new Persona("Maria", "mari@gmail.com", 24, 1);
        Persona pers2 = new Persona("Mario", "mario@gmail.com", 40, 2);

        PersonaController person = new PersonaController();

        person.muestraPersona(pers2);
        person.comparaPersona(pers1, pers2);



        
        
    }
}


