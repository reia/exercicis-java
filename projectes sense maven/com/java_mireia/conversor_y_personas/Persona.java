package com.java_mireia.conversor_y_personas;

// Nombre y edad deberían ser privados para que no se cambien desde otros lugares. En otras clases se pueden importar estos objetos, y si son públicos, podrían cambiar el valor de estos objetos desde otras clases. 

public class Persona {
    private String nombre;
    private String email;
    private int edad;
    private int id;

    public Persona (String nombre, String email, int edad, int id) {
            this.nombre = nombre;
            this.edad = edad;
            this.email = email;
            this.id = id;
    }

    /**
     * @return the nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * @param nombre the nombre to set
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return the edad
     */
    public int getEdad() {
        return edad;
    }

    /**
     * @param edad the edad to set
     */
    public void setEdad(int edad) {
        this.edad = edad;
    }

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    // Al comparar objetos con == solo se fija donde están apuntando, no si en las variables de dentro son iguales o no. Para poder comparar se tiene que sobreescribir un método llamado equals para que la función sepa como comparar. Para comparar Strings TAMBIÉN necesitamos usar el método equals. 

    // @Override
    // public boolean equals(Object ob){
    //     Persona otro= (Persona) ob;
    //     if(this.edad==otro.edad){
    //         return true;
    //     }else{
    //         return false;
    //     }
    // }

}  

    // Esta función sobreescribe al método toString por defecto. 

    // @Override 
    // public String toString() {
    //     return this.nombre+" ("+this.edad+")";
    // }

   