package com.java_mireia.numero;

import java.util.Scanner;

class PideNums {
    public static void pide() {
    
    Scanner keyboard = new Scanner(System.in);
    int total = 0;
    int num=0;
    int contador=0;
    boolean primeraVez= true;

    InfoNums info = new InfoNums();
    
    do {
        System.out.printf("Entra un num: ");
        try {  

                num = keyboard.nextInt();
                if (primeraVez) {
                    info.setMax(num);
                    info.setMin(num);
                    primeraVez= false;
                }
                info.setMax(num);
                info.setMin2(num);
                total += num; 
                contador++;
            } catch (Exception e) {  
                System.out.println("***Dato incorrecto - 0 para salir***");
                keyboard.next();
            } 
    } while (num>0);
    System.out.println("La suma es "+ total);
    keyboard.close();

    info.setCantidad2(contador);
    info.setMedia(total);

    System.out.println(info.getCantidad()+ " números introducidos \n número mayor: "+info.getMax()+"\n número menor: "+info.getMin()+"\n media aritmética: "+info.getMedia());
}

}
