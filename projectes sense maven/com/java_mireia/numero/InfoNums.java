
package com.java_mireia.numero;


class InfoNums{
    private int max= 0;
    private int cantidad;
    private int min= 99999;
    private double media;

    /**
     * @return the max
     */
    public int getMax() {
        return max;
    }

    

    /**
     * @param max the max to set
     */
    public void setMax(int max) {
        if(max>this.max) {
            this.max = max;
        }
    }

   

    /**
     * @return the cantidad
     */
    public int getCantidad() {
        return cantidad;
    }

    /**
     * @param cantidad the cantidad to set
     */
    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    // Cantidad exercici 2

    public void setCantidad2(int cantidad) {
        this.cantidad = cantidad-1;
    }

    /**
     * @return the min
     */
    public int getMin() {
        return min;
    }

    /**
     * @param min the min to set
     */
    public void setMin(int min) {
        if(min<this.min){
            this.min= min;
        } 
    }

    // Minim per exercici 2

    public void setMin2(int numero) {
        
        if(numero != 0) {
            if(numero<this.min){
                this.min= numero;
            }else { this.min= 0;

            }
            } 
        
    }

    /**
     * @return the media
     */
    public double getMedia() {
        return media;
    }

    /**
     * @param media the media to set
     */
    public void setMedia(int total) {
        this.media= total*1.0/this.cantidad;
    }

    



}
