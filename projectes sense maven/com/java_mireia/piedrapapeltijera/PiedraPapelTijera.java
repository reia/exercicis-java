// Para crear jar: java -jar Test.jar

package com.java_mireia.piedrapapeltijera;

import java.util.Scanner;
import java.util.Random;

class PiedraPapelTijera {

    public static void juega() {

        Scanner keyboard = new Scanner(System.in);
        Random rand = new Random();

        int contJugador = 0;
        int contMaquina = 0;
        String maquina = "";
        String jugador = "";

        do {

            int randomNumber = rand.nextInt(3) + 1;
            if (randomNumber == 1) {
                maquina = "piedra";
            } else if (randomNumber == 2) {
                maquina = "papel";
            } else if (randomNumber == 3) {
                maquina = "tijera";
            }


            do {

            System.out.printf("Escribe tu opción (piedra, papel o tijera): ");

            jugador = keyboard.nextLine();

            if(jugador.equals("Piedra") || jugador.equals("Papel") || jugador.equals("Tijera")) {
                jugador = jugador.toLowerCase();

            }            

        } while(!jugador.equals("piedra") && !jugador.equals("papel") && !jugador.equals("tijera"));
                
            

            if (maquina.equals("piedra") && jugador.equals("papel")) {
                contJugador++;
                System.out.println("Has ganado esta ronda! La máquina ha sacado piedra");
            } else if (maquina.equals("papel") && jugador.equals("piedra")) {
                contMaquina++;
                System.out.println("Has perdido esta ronda! La máquina ha sacado papel");
            }

            if (maquina.equals("tijera") && jugador.equals("piedra")) {
                contJugador++;
                System.out.println("Has ganado esta ronda! La máquina ha sacado tijera");
            } else if (maquina.equals("piedra") && jugador.equals("tijera")) {
                contMaquina++;
                System.out.println("Has perdido esta ronda! La máquina ha sacado piedra");
            }

            if (maquina.equals("papel") && jugador.equals("tijera")) {
                contJugador++;
                System.out.println("Has ganado esta ronda! La máquina ha sacado papel");
            } else if (maquina.equals("tijera") && jugador.equals("papel")) {
                contMaquina++;
                System.out.println("Has perdido esta ronda! La máquina ha sacado tijera");
            }

            if (maquina.equals(jugador)) {
                System.out.println("Empate! Nueva ronda!");
            }

            System.out.println(contJugador);
            System.out.println(contMaquina);

        } while (contJugador < 3 && contMaquina < 3);

        keyboard.close();

        if (contJugador < contMaquina) {
            System.out.println("Has perdido. Máquina: " + contMaquina + "\n Jugador: " + contJugador);
        } else {
            System.out.println("Has ganado. Máquina: " + contMaquina + "\n Jugador: " + contJugador);
        }

    }
}