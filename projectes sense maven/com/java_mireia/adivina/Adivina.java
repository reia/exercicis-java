
package com.java_mireia.adivina;

import java.util.Scanner;
import java.util.Random;

class Adivina {

public static void main(String[] args) {

    Random rand = new Random(); 
    // Siempre da un número entre 0 y el número entre paréntesis. Se tiene que añadir 1 para que se pida del 1 al 10;
    int randomNumber = rand.nextInt(100) + 1;
    
    Scanner keyboard = new Scanner(System.in);
    
    int num=0;
    int contador=0;
    do {
        System.out.printf("Entra un num entre el 1 y el 100: ");
        try {  
                num = keyboard.nextInt();
                contador++; 
                if (num == randomNumber) {
                    System.out.println("Felicidades! Has acertado el número en "+contador+" intentos!");
                } else if (num<randomNumber) {System.out.println("Intenta con un número más alto");} else {
                    System.out.println("Intenta con un número más bajo"); }
                 
            } catch (Exception e) {  
                System.out.println("***Dato incorrecto - 0 para salir***");
                keyboard.next();
            } 
    } while (num!=randomNumber);
    
    keyboard.close();
}

}
