import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.BufferedWriter;

public class Filtra {
	public static void main(String[] args) {
		if (args.length==0) {
			System.out.println("Archivo no especificado.");
			return;
		}
		String archivo = args[0].toLowerCase();
		File fin= new File("motos.csv");
		int lineas = 0;
		File dhFile = new File("hondaMotos.txt");
		
		try (	FileReader fr = new FileReader(fin);
				BufferedReader br = new BufferedReader(fr);
				FileWriter fw = new FileWriter(dhFile);
				BufferedWriter bw = new BufferedWriter(fw);
				) {
			String line;
			do {
				line = br.readLine();
					System.out.println(line);
					

				if (line != null) {
					if (line.contains("HONDA")) {
						bw.write(line);
						bw.newLine();
						lineas++;
						}
						}

			
			} while (line!=null);

			bw.flush();
			bw.close();
	
		} catch (Exception e) {
			e.printStackTrace();
		} 
		System.out.printf("El arhivo %s tiene %d lineas.", archivo, lineas);

			
}
}