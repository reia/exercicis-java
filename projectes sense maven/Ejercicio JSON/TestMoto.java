import com.google.gson.Gson;
import java.io.File;
import java.io.FileReader;
import java.io.BufferedReader;
import java.io.StringWriter;



public class TestMoto {
    public static void main(String[] args) {
       
            File dhFile = new File("motos.json");
           try ( FileReader fr = new FileReader(dhFile);
            BufferedReader br = new BufferedReader(fr);
            StringWriter sw = new StringWriter(); ) {
            int size = 100;
            char[] buffer = new char[size];
            int len;
            while ((len = br.read(buffer, 0, size)) > -1) {
            sw.write(buffer, 0, len);
            }
            br.close();
           

        
        String str = sw.toString();
        Gson gson = new Gson();
    
       
        Moto[] motosImport = gson.fromJson(str, Moto[].class);

    
        int contaScoopy = 0;
        int precioScoopy = 0;
        for (Moto x: motosImport) {
            System.out.println(x);
            
            if (x.model.contains("SCOOPY")) {
                contaScoopy++;
                precioScoopy= precioScoopy + x.preu;
            }

            
        }

        System.out.println("Hay "+contaScoopy+" motos del modelo Scoopy");
        System.out.println("El precio medio de la Scoopy es "+precioScoopy/contaScoopy+".");

        

        } catch (Exception e) { 
            System.out.println(e);
            //e.printStackTrace();
        }

         

    
    }
}