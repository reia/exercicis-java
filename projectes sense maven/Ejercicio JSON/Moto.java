public class Moto {
    String model;
    int preu;
    String kilometres;
    int cilindrada;
    String lloc;
    int any;

    public Moto(String model, int preu, String kilometres, int cilindrada, String lloc, int any) {
        this.model = model;
        this.preu = preu;
        this.kilometres = kilometres;
        this.cilindrada = cilindrada;
        this.lloc = lloc;
        this.any = any;
    }

    @Override
    public String toString() {
        return "Moto [any = " + this.any + ", cilindrada = " + this.cilindrada + ", kilometres = " + this.kilometres + ", lloc=" + lloc
                + ", model = " + this.model + ", preu = " + this.preu + "]";
    }
}