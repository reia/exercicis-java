// Para que no se lie, los métodos que no queremos cambiar se suelen escribir como final. 

class TestGenerico {

    public static void main (String[] args) {

    // Es crea un Xml del Generic. Al funcionar ens agafarà el métode que hem definit a Xml. Mai es podrà fer un new Generico! 
    // (No es poden fer new ni a classes abstractes ni a interfaces.)
        Generico gen = new Xml();

        String s1= gen.execute(8);
        System.out.println(s1);
        s1 = gen.execute(17);
        System.out.println(s1);
    // Es canvia a Json. Llavors al cridad la mateixa función ens agafarà el métode definit a Json.  
        gen = new Json();

        String s2 = gen.execute(8);
        System.out.println(s2);
        s2 = gen.execute(17);
        System.out.println(s2);
        
    }
}