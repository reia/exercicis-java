import java.io.*;
import java.net.*;

public class Bicing {
    public static void main(String[] args) {
      
        Bicing.conecta();
       
}

// El throws lo que hace es devolver el error. Entonces te dará el error arriba y por lo tanto se tendrá que manejar desde arriba.
// Si quieres manejar todas las excepciones de golpe, se puede poner Exception directamente.
// public static void conecta () throws Exception {

public static void conecta() {
    File dest = new File("bicing.json");
    URLConnection conn = null;
        
       try { URL url = new URL("https://api.citybik.es/v2/networks/bicing");
        conn = url.openConnection(); 

// Forma de poner las dos excepciones juntas. 
    // } catch (MalformedURLException | IOException e) {

// Poner las excepciones por separado para que cada una imprima una cosa. Mejor método para gestionar los errores.

// También podríamos anidar try catch. Eso nos aseguraría que el segundo no se ejecuta si ha habido algún error en el primero. 
    } catch (MalformedURLException e) {
        System.out.println("URL no válida");
        return;
    } catch (IOException e) {
        System.out.println("La pàgina no responde");
        return;
    }
       
        try ( 
           
        InputStream is = conn.getInputStream();
        BufferedInputStream bis = new BufferedInputStream(is);
        FileOutputStream fos = new FileOutputStream(dest);
        BufferedOutputStream bos = new BufferedOutputStream(fos); ) {
        int size = 1024;
        byte[] buffer = new byte[size];
        int len;
       
        while ((len = bis.read(buffer, 0, size)) > -1) {
        bos.write(buffer, 0, len);
        }
        bos.flush();
        bis.close();
        bos.close();
            

        } catch (Exception e) {
			e.printStackTrace();
		} 
        
}
}