public class PintadorTabs {

    static String pintaTab(Pintable cosa) {
        // Instanceof nos indica si es del tipo Producto. Devuelve true si lo es y falso si no. 
        if (cosa instanceof Producto) {

            // Casteo. Convertimos esta cosa en producto. Lo podemos hacer porque sabemos seguro que es un producto (por el if).
            Producto p1 = (Producto) cosa;
            return p1.getDescription();
            
            
        } else if (cosa instanceof Factura) {
            Factura f1 = (Factura) cosa;
            return f1.getNumeroFactura()+"";

        } else if (cosa instanceof Cliente) {

            return ((Cliente) cosa).getNombre();
        } else {
            return "Error";
        }

    }
}