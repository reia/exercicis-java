class Test {
    public static void main(String[]arg) {
        Producto p = new Producto("paquete de folios");
        Cliente c= new Cliente("Toni Lopez");
        Factura f= new Factura (1234);

        System.out.println( PintadorTabs.pintaTab(p));
        System.out.println( PintadorTabs.pintaTab(c));
        System.out.println( PintadorTabs.pintaTab(f));

        Class x = p.getClass();

        // Te dice el tipo de Clase que es.
        System.out.println(x.getName());
     
    }
}
