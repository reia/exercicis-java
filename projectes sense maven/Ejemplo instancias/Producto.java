public class Producto implements Pintable {

    private String description;

    public Producto(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }
}