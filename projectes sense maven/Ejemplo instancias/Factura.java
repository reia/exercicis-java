public class Factura implements Pintable {

    int numeroFactura;

    public Factura(int num) {
        this.numeroFactura = num;
    }

    public int getNumeroFactura() {
        return numeroFactura;
    }
}