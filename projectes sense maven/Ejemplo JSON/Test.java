import com.google.gson.Gson;


 public class Test {
    public static void main(String[] args) {
       Gson gson = new Gson();

//         Persona p = new Persona("humbert", 22);
//         gson.toJson(p, System.out);
        
//         String str = gson.toJson(p);
//         System.out.println(str);

//         String js = "{\"nombre\":\"humbert\",\"edad\":22}";

//         Persona p2 = gson.fromJson(js, Persona.class);
//         System.out.println(p2.nombre);


        Persona con1 = new Persona("ana", 22);
        Persona con2 = new Persona("maripili", 65);
        Persona con3 = new Persona("pesado", 20);
        Persona con4 = new Persona("marti", 33);

        Persona[] contactos = {con1, con2, con3, con4};

        String str = gson.toJson(contactos);

        System.out.println(str);

        Persona[] contactosImportados = gson.froJson(str, Persona[].class);
        for (Persona x: contactosImportados) {
            System.out.println(x.toString());
        }
    }
}
