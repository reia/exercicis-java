<%@page contentType="text/html; charset=UTF-8" %>
<%@page import="com.enfocat.java.Pipati" %>
<%@page import="com.enfocat.java.HtmlFactory" %>
<%@page import="com.enfocat.java.Icon" %>
<%
HtmlFactory factory = new HtmlFactory();
Icon icono = new Icon();

int jugada_usuario = Integer.parseInt(request.getParameter("jugada")); //1
String respuesta = Pipati.partida(jugada_usuario); //2
int maquina= 0;


if  (respuesta.equals("Empate")) {
    maquina = jugada_usuario;
} else if (respuesta.equals("Ganas") && jugada_usuario == 1) {
    maquina = 3;
} else if (respuesta.equals("Ganas") && jugada_usuario == 2) {
    maquina = 1;
} else if(respuesta.equals("Ganas") && jugada_usuario == 3) {
    maquina = 2;
} else if( respuesta.equals("Pierdes") && jugada_usuario ==1) {
    maquina = 2;
} else if ( respuesta.equals("Pierdes") && jugada_usuario== 2) {
    maquina = 3;
} else if ( respuesta.equals("Pierdes") && jugada_usuario == 3) {
    maquina = 1;
}


System.out.println(maquina);



%>
<!DOCTYPE html>
<html lang="es-ES">
<head>
<meta charset="utf-8">
<title>Java demo</title>
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css"
        integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
<link href="https://fonts.googleapis.com/css?family=Shadows+Into+Light&display=swap" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="css/estilos.css">
</head>
<body>

    <div class="titulo">  <%= factory.titulo(respuesta) %> </div>

   <div class="iconContainer container2"> <div class="effectleft"><%= icono.calcula(jugada_usuario) %></div>

    <div class="effectright"> <%= icono.calcula(maquina) %> </div>

    

   
     


<a href="index.jsp">Volver a jugar</a>
</body>
</html>