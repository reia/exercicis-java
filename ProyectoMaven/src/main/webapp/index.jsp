<%@page contentType="text/html; charset=UTF-8" %>
<%@page import="com.enfocat.java.HtmlFactory" %>

<%
    HtmlFactory factory = new HtmlFactory();
%>
<!DOCTYPE html>
<html lang="es-ES">
<head>
<meta charset="utf-8">
<title>Java demo</title>
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css"
        integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
<link href="https://fonts.googleapis.com/css?family=Shadows+Into+Light&display=swap" rel="stylesheet">

<link rel="stylesheet" type="text/css" href="css/estilos.css">
</head>
<body>
<div class="titulo"><%= factory.titulo("Piedra, Papel o Tijera?") %></div>
<div class="iconContainer">
<a href="jugada.jsp?jugada=1"><i class="far icons1 fa-4x rock fa-hand-rock"></i></a>
<a href="jugada.jsp?jugada=2"><i class="far icons1 fa-4x paper fa-hand-paper"></i></a>
<a href="jugada.jsp?jugada=3"><i class="far icons1 fa-4x scissors fa-hand-scissors"></i></a>
</div>
</body>
</html>