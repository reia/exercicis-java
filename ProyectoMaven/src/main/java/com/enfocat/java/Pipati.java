package com.enfocat.java;

import java.util.Random;

public class Pipati {
private static String[] jugadas = {"Piedra", "Papel", "Tijera"};

public static String partida(int jugada_usuario){
String respuesta = "";
Random rnd = new Random();
int jugada_ordenador = rnd.nextInt(3)+1;
if (jugada_usuario==jugada_ordenador) {
respuesta = String.format("Empate");

} else if (jugada_usuario==1 && jugada_ordenador==2){
respuesta = String.format("Pierdes");

} else if (jugada_usuario==1 && jugada_ordenador==3){
respuesta = String.format("Ganas"); 

} else if (jugada_usuario==2 && jugada_ordenador==1){
respuesta = String.format("Ganas");

} else if (jugada_usuario==2 && jugada_ordenador==3){
respuesta = String.format("Pierdes");

} else if (jugada_usuario==3 && jugada_ordenador==1){
respuesta = String.format("Pierdes");

} else if (jugada_usuario==3 && jugada_ordenador==2){
respuesta = String.format("Ganas");
}
return respuesta;
}


}