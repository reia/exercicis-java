import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Random;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.HashMap;
import java.util.TreeMap;

public class Flota {

    static List<Coche> coches = new ArrayList<Coche>();
    String[] colores = { "blanco", "negro", "turquesa", "azul", "gris", "rojo", "granate", "violeta", "plateado",
            "dorado", "gris oscuro", "verde claro", "verde oscuro", "azul oscuro", "amarillo", "naranja", "marron",
            "coral", "rosa", "beige", "ocre" };
    String[] marcas = { "renault", "dacia", "citroen", "seat" };

    public void generaCoches(int num) {

        Random rand = new Random();
        String colorValue;
        String brandValue;
        int kms;
        int year;

        for (int i = 0; i < num; i++) {

            int randomNumber = rand.nextInt(colores.length);
            colorValue = colores[randomNumber];

            int randomNumber2 = rand.nextInt(marcas.length);
            brandValue = marcas[randomNumber2];

            kms = rand.nextInt(150000);

            year = rand.nextInt(20) + 2000;

            Coche coche = new Coche(brandValue, colorValue, kms);

            coches.add(coche);
        }

    }

    public void consultaMarca(String marca) {

        int contador = 0;

        for (int i = 0; i < coches.size(); i++) {
            if (coches.get(i).getMarca().equals(marca)) {
                contador++;
                System.out.println(coches.get(i).toString());
            }
        }
        System.out.println("El total de coches de la marca " + marca + " es: " + contador);
    }

    void consultaKm(int minkm) {

        int contador = 0;
        for (int i = 0; i < coches.size(); i++) {
            if (coches.get(i).getKms() >= minkm) {
                contador++;
                System.out.println(coches.get(i));

            }
        }

        System.out.println("El total de coches con " + minkm + " o más es: " + contador);
    }

    public void consultaColor(String color) {

        int contador = 0;
        for (int i = 0; i < coches.size(); i++) {
            if (coches.get(i).getColor().equals(color)) {
                contador++;
                System.out.println(coches.get(i).toString());
            }
        }

        System.out.println("El total de coches de color " + color + " es: " + contador);
    }

    void consultaMultiple(String color, String marca, int minkm) {
        int contador = 0;
        for (int i = 0; i < coches.size(); i++) {
            if (coches.get(i).getColor().equals(color)) {
                if (coches.get(i).getMarca().equals(marca)) {
                    if (coches.get(i).getKms() >= minkm) {
                        contador++;
                        System.out.println(coches.get(i));
                    }
                }
            }
        }

        System.out.println("Hemos encontrado " + contador + " coches con las características seleccionadas.");
    }

    // List<Integer> nums = Arrays.asList(3, 1, 2, 2, 4, 5, 4, 2);
    // Map<Integer, AtomicInteger> numMap =

    // new TreeMap<Integer, AtomicInteger>();

    // Primer busca el primer element. Com no el troba, si es null el crea i el
    // passa a map, per lo tant ja
    // té un valor de 3. Quan arriba al segon dos, com count no és igual a null
    // perquè ja hi ha un 2, puja el valor del contador. Si és un Atomic Integer es pot canviar el
    //  número del globo sense substituir-lo per un de nou cada vegada que el vulguem canviar.

    // for (Integer num : nums) {
    // AtomicInteger count = numMap.get(num);
    // if (count == null) {
    // count = new AtomicInteger(0);
    // numMap.put(num, count);
    // }
    // count.incrementAndGet();
    // }
    // for (Map.Entry<Integer, AtomicInteger> entry : numMap.entrySet()) {
    // System.out.print("Ocurrencias de " + entry.getKey() + ": ");
    // System.out.println(entry.getValue());

    public void marcaMaxima() {
        int marcasMaxCount= 0;
        String marcaRepe="";
        String marcaRepeEmpate= "";
        Boolean empate= false;
       

        Map<String, AtomicInteger> cuentamarca = new TreeMap<String, AtomicInteger>();

        for (int i = 0; i < coches.size(); i++) {
            String marca = coches.get(i).getMarca();
                AtomicInteger count = cuentamarca.get(marca);

                if (count == null) {
                    count = new AtomicInteger(0);
                    cuentamarca.put(marca, count);
                    count.incrementAndGet();  
                } else { count.incrementAndGet();
                }
            }    

        for (Map.Entry<String, AtomicInteger> parella : cuentamarca.entrySet()) {
            String marca = parella.getKey();
            int qtt = parella.getValue().get();

            if( qtt == marcasMaxCount){
                marcaRepeEmpate = marca;
                empate = true;
            }

            if( qtt > marcasMaxCount) {
                marcasMaxCount = qtt;
                marcaRepe = marca;
                empate = false;
            }

            
        }

        if (empate == false) {
        System.out.println("La marca que más se repite es el "+marcaRepe+" y se repite "+marcasMaxCount+" veces"); }
        if (empate == true) {
        System.out.println("Las marcas que más se repiten son: "+marcaRepe+" y "+marcaRepeEmpate+". Se repiten "+marcasMaxCount+" veces.");
        }

        

    }

    public void colorMaximo() {
        int coloresMaxCount= 0;
        String colorRepe="";
        String colorRepeEmpate= "";
        Boolean empate= false;
       

        Map<String, AtomicInteger> cuentaColor = new TreeMap<String, AtomicInteger>();

        for (int i = 0; i < coches.size(); i++) {
            String color = coches.get(i).getColor();
                AtomicInteger count = cuentaColor.get(color);

                if (count == null) {
                    count = new AtomicInteger(0);
                    cuentaColor.put(color, count);
                    count.incrementAndGet();
                    
                    
                } else { count.incrementAndGet();
                }

            }    

        for (Map.Entry<String, AtomicInteger> parella : cuentaColor.entrySet()) {
            String color = parella.getKey();
            int qtt = parella.getValue().get();

            if( qtt == coloresMaxCount ){
                colorRepeEmpate = color;
                empate = true;
            }

            if( qtt > coloresMaxCount) {
                coloresMaxCount = qtt;
                colorRepe = color;
                empate = false;
            }

            
        }

        if (empate == false) {
        System.out.println("El color que más se repite es "+colorRepe+" y se repite "+coloresMaxCount+" veces"); }
        if (empate == true) {
        System.out.println("Los colores que más se repiten son: "+colorRepe+" y "+colorRepeEmpate+". Se repiten "+coloresMaxCount+" veces.");
        }
    }

    public void resumen() {
        int numCoches =0;
        int cochesKms= 0;
        for(int i=0; i< coches.size() ; i++) {
            numCoches++;
            if (coches.get(i).getKms()> 100000) {
                cochesKms++;
            }
        }
        System.out.println("Hay "+numCoches+" coches en la Lista.");
        colorMaximo();
        marcaMaxima();
        System.out.println(cochesKms+" tienen más de 100.000 Kilometros");

       
    }

    public static void main(String[] args) {
        Flota prueba = new Flota();

        prueba.generaCoches(20);

        System.out.println(coches.toString());

        prueba.consultaMarca("seat");

        prueba.consultaColor("azul oscuro");

        prueba.consultaKm(100000);

        prueba.consultaMultiple("plateado", "citroen", 10000);

        prueba.resumen();
    }

}