public class Coche {
    String marca;
    String color;
    int kms;
   

    public Coche(String marca, String color, int kms) {
        this.marca = marca;
        this.color = color;
        this.kms = kms;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public int getKms() {
        return kms;
    }

    public void setKms(int kms) {
        this.kms = kms;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((color == null) ? 0 : color.hashCode());
        result = prime * result + kms;
        result = prime * result + ((marca == null) ? 0 : marca.hashCode());
        return result;
    }

    // @Override
    // public boolean equals(Object obj) {
    //     if (this == obj)
    //         return true;
    //     if (obj == null)
    //         return false;
    //     if (getClass() != obj.getClass())
    //         return false;
    //     Coche other = (Coche) obj;
    //     if (color == null) {
    //         if (other.color != null)
    //             return false;
    //     } else if (!color.equals(other.color))
    //         return false;
    //     if (kms != other.kms)
    //         return false;
    //     if (marca == null) {
    //         if (other.marca != null)
    //             return false;
    //     } else if (!marca.equals(other.marca))
    //         return false;
    //     return true;
    // }

    @Override 
    public String toString() {
      return this.marca+" (Color: "+this.color+") (Kilometros: "+this.kms+")\n";
     }

   
}