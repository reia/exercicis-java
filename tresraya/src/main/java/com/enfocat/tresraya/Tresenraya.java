package com.enfocat.tresraya;

import java.util.Scanner;
import java.util.Random;

public class Tresenraya {

    int[] map = { 0, 0, 0, 0, 0, 0, 0, 0, 0 };

    public void draw() {

        for (int i = 0; i < map.length; i++) {
            System.out.print(map[i]);
            if (i == 2 || i == 5 || i == 8) {
                System.out.println("\n");
            }
        }
    }

    public int getMap(int n) {
        int value = map[n];
        return value;

    }

    public boolean setMap(int position, Jugador jugador) {

        if(jugador.nombre.equals("jugador")) 
        {map[position] = 1;} else 
        {map[position]= 2;}
        boolean comprueba = false;

        for(int[]combinacion:WINNERS) {
            if( getMap(combinacion[0])== 
            getMap(combinacion[1]) && getMap(combinacion[1]) == getMap(combinacion[2]) && getMap(combinacion[0])!= 0)
            { if (map[position] == 1)
                { System.out.println("Felicidades! Has ganado");
                    comprueba = true;
                    } else if (map[position] == 2) { 
                        System.out.println("Mala Suerte! Te ha ganado la maquina.");
                        comprueba = true;
                }

            } 

        }

        return comprueba;

    }

    private static int[][] WINNERS = new int [][]{
        
        {0,1,2},
        {3,4,5},
        {6,7,8},
        {0,3,6},
        {2,5,8},
        {1,4,7},
        {0,4,8},
        {2,4,6}

        };


    public int numZeros() {
        int contador = 0;

        for (int i = 0; i < map.length; i++) {
            if (map[i] == 0) {
                contador++;
            }
        }
        return contador;
    }

    public void play() {

        Scanner keyboard = new Scanner(System.in);

        int position = 10;
        boolean ganador = false;
        Jugador jug1 = new Jugador();
        jug1.nombre= "jugador";
        Jugador maquina = new Jugador();
        maquina.nombre= "maquina";

        do {

            do {
                do {
                System.out.println("Elige una posición del 0 al 8:");
                position = keyboard.nextInt();

                } while (position > 8);
                
                if (numZeros() != 0) {
                    if (map[position] != 0) {
                        System.out.println("Esta posición ya está ocupada! Elige otra por favor");
                    }
                }
            } while (map[position] != 0);

            if(setMap(position, jug1) == true) {
                ganador= true;
                draw();
                break;
            }

            

            Random rand = new Random();
            int number = 10;

            do {

                number = rand.nextInt(8);

            } while (map[number] != 0);

            if (setMap(number, maquina) == true) {
                ganador= true;
                draw();
                break;
            }

            System.out.println("La máquina ha elegido:");
            draw();

        } while (numZeros()!= 0);

        if(numZeros()==0 && ganador== false) {
            System.out.println("Se acabó la partida! Empate!!");
        }

        keyboard.close();
    }

}
