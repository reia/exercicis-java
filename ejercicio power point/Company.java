

public class Company {
    private String name;
    private int year;

    public Company (String name, int year) {
        this.name = name;
        this.year = year;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        result = prime * result + year;
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
    // Como vamos a utilizarlo con la clase Company, cambiamos el objeto general que viene de arriba a un Company. 
        Company other = (Company) obj;
        if (name == null) {
            if (other.name != null)
                return false;
        } else if (!name.equals(other.name))
            return false;
        if (year != other.year)
            return false;
        return true;

        // OVERRIDE MANUAL:

        // @Override
        // public boolean equals(Object obj) {
        //     Company otra= (Company) obj;
        //     if (this.name.equals(otra.name ) && this.year == otra.year) {
        //         return true;
        //     } else { return false;}
            
        // }
     }
    
}