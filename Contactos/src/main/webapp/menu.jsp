<%@page contentType="text/html;charset=UTF-8" %>

<link rel="stylesheet" type="text/css" href="/contactos/css/estilos.css">


<nav class="navbar navbar-expand-lg  navbar-dark">
    <a class="navbar-brand" href="#">Contactos</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
  
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav mr-auto">

    
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Agregar
          </a>
          <div class="dropdown-menu" aria-labelledby="navbarDropdown">
            <a class="dropdown-item" href="/contactos/contacto/crea.jsp">Nuevo Contacto</a>
            <a class="dropdown-item" href="/contactos/llamadas/creaLlamada.jsp">Nueva Llamada</a>
          </div>
        </li>
         <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Listados
          </a>
          <div class="dropdown-menu" aria-labelledby="navbarDropdown">
            <a class="dropdown-item" href="/contactos/contacto/list.jsp">Listado de Contactos</a>
            <a class="dropdown-item" href="/contactos/llamadas/listLlamada.jsp">Listado de Llamadas General</a>
           
          </div>
        </li>

      </ul>

    </div>
  </nav>
  
