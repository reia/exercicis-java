<%@page contentType="text/html;charset=UTF-8" %>
<%@page import="com.enfocat.mvc.*" %>
<%@page import="java.text.*" %>
<%@page import="java.util.Date" %>
<%@page import="java.util.ArrayList" %>


<%

    Llamada llam = null;
    boolean datos_guardados=false;
    DBDatos dato = new DBDatos();
    ContactoController contacto = new ContactoController();
   

    //IMPORTANTISIMO!!! ANTES DE CUALQUIER GETPARAMETER!!!    
    request.setCharacterEncoding("UTF-8");

    String id = request.getParameter("id");
    System.out.println(id);
    if (id==null){
      
        //no recibimos id, debe ser un error... volvemos a index
        response.sendRedirect("/contactos");
        //IMPORTANTE! después de sendRedirect poner un RETURN!!!
        return;
    }
    
%>


<!DOCTYPE html>
<html lang="es-ES">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>ContactosApp</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css"
        integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="/contactos/css/estilos.css">
</head>
<body>

<%-- Permite insertar un jsp dentro de otro. En este caso se importa el menú. --%>
<%@include file="/menu.jsp"%>

<div class="container">

<div class="row">
<div class="col">


        <% 

        Contacto contactoSeleccionado= contacto.getContactById(Integer.parseInt(id));

        String nombreContacto= contactoSeleccionado.getNombre();
   
        
        %> 

<h1>Listado de Llamadas de <%=nombreContacto%></h1>
</div>
</div>

<div class="row">
<div class="col">


<table class="table" id="tabla_listado">
  <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">Fecha</th>
      <th scope="col">Notas</th>
      <th scope="col">Editar</th>
      <th scope="col">Eliminar</th>

      
      
    
    </tr>
  </thead>
  <tbody>
  <% 
     DBDatosLlamada datos = new DBDatosLlamada();%>
  
    <% for(Llamada llamada : datos.getLlamadasContacto(Integer.parseInt(id))) { %>
        <tr>
        <th scope="row"><%= llamada.getId() %></th>
        <td><%= llamada.getFecha() %></td>
        <td><%= llamada.getNotas() %></td>
        <td><a href="/contactos/llamadas/editaLlamada.jsp?id=<%= llamada.getId() %>&idContacto=<%= id %> "><i class="edita far fa-edit"></a></td>
        <td><a href="/contactos/llamadas/eliminaLlamada.jsp?id=<%= llamada.getId() %>"><i class="delete far fa-trash-alt"></i></a></td>
      </tr>

   <%}%>


        
  </tbody>
</table>

<%-- Lleva a contacto.jsp para crear un nuevo contacto. --%>
<a href="/contactos/llamadas/creaLlamada.jsp?id=<%= Integer.parseInt(id) %>" class="btn btn-sm btn">Nueva llamada de <%=nombreContacto%></a>
</div>
</div>

</div>
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>
    <script src="/contactos/js/scripts.js"></script>
</body>
</html>
