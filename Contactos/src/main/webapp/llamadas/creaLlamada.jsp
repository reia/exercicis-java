<%@page contentType="text/html;charset=UTF-8" %>
<%@page import="com.enfocat.mvc.*" %>
<%@page import="java.text.*" %>
<%@page import="java.util.Date" %>

<%

    boolean datosOk;
    DBDatos datos = new DBDatos();
    ContactoController contacto = new ContactoController();
    int idContacto= 0;
    String contactos ="";
    String contactoNombre= "";

    request.setCharacterEncoding("UTF-8");

    String id = request.getParameter("id");

    // Es posible que lleguemos aquí desde index principal, con una llamada GET
    // o bien como resultado del envío del formulario, con una llamada POST
    
    // si es un POST... (IgnoreCase es para que de igual si es en mayúscula o minúscula).
    if ("POST".equalsIgnoreCase(request.getMethod())) {
        // hemos recibido un POST, deberíamos tener datos del nuevo contacto. Al pasar por POST todos los datos se convierten en String!
    
        //IMPORTANTISIMO!!! ANTES DE CUALQUIER GETPARAMETER!!!  
        request.setCharacterEncoding("UTF-8");


        String fecha = request.getParameter("fecha");
        String notas = request.getParameter("notas");
        String nombreContacto= request.getParameter("nombreContacto");
        

          DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            Date formatoFecha = sdf.parse(fecha);
            System.out.println(formatoFecha);
            fecha = sdf.format(formatoFecha);
          

        String idContactoS = request.getParameter("idContacto");
       
            if( idContactoS != null) {
                 idContacto = Integer.parseInt(idContactoS);
            } else {

                for (Contacto cn : DBDatos.getContactos()){
                    if (cn.getNombre().equals(nombreContacto)){
                        idContacto = cn.getId();
                    }
                }
            }

            
        

      

        // aquí verificaríamos que todo ok, y solo si todo ok hacemos SAVE, 
        // por el momento lo damos por bueno...
        datosOk=true;
        if (datosOk){
            Llamada a = new Llamada(fecha,notas,idContacto);
            LlamadaController.save(a);
            // nos vamos a mostrar la lista, que ya contendrá el nuevo contacto.
            response.sendRedirect("/contactos/llamadas/listLlamada.jsp");
            return;
        }
    } else {


    

    if (id == null) {

        for (Contacto cn : DBDatos.getContactos()){
               contactos += String.format("<option>%s</option>",  cn.getNombre());
               // contactos += String.format("<option value='%d'>%s</option>", cn.getId(), cn.getNombre()); 
            }
    } else {

         if (id!= null) {
         idContacto = Integer.parseInt(id);
          Contacto contactoSeleccionado= contacto.getContactById(idContacto);
          String nombreContactoSelec= contactoSeleccionado.getNombre();
          contactoNombre= nombreContactoSelec;
          System.out.println(contactoNombre);
    }

    }

    }
%>


<!DOCTYPE html>
<html lang="es-ES">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>AcademiaApp</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="/contactos/css/estilos.css">
</head>
<body>

<%@include file="/menu.jsp"%>

<div class="container">
<div class="row">
<div class="col">

<% if (id== null) { %>
<h1>Nueva llamada</h1>
<% } %>

<% if (id!= null) { %>
<h1>Nueva llamada de <%=contactoNombre%></h1>
<% } %>
</div>
</div>


<div class="row">
<div class="col-md-8">

<form id="formcrea" action="#" method="POST">
  <div class="form-group">
    <label class= "labelForm" for="fechaInput">Fecha de la llamada</label>
    <input  name="fecha"  type="date" class="form-control" id="fechaInput" >
  </div>
  
 <div class="form-group">
    <label class= "labelForm" for="notasInput">Notas</label>
    <input  name="notas"  type="text" class="form-control" id="notasInput">
  </div>

  <div class="form-group">
    <label for="contactoInput">Contacto</label>
    <% if(id == null) {%>
    <select name="nombreContacto"  type="text" class="form-control" id="nombreContacto" >
    <%=contactos%>
    </select>
    <%}%>
    <% if(id != null) {%>
    <input  name="nombreContacto"  type="text" class="form-control" id="nombreContacto" value="<%=contactoNombre%>" disabled>
    <input type="hidden" name="idContacto" value="<%= id %>">
    <%}%>

  
    <button type="submit" class="btn btn">Guardar</button>
</form>


</div>
</div>


    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>

</body>
</html>
