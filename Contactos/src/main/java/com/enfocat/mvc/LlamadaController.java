package com.enfocat.mvc;

import java.util.List;


public class LlamadaController {
    
    static final String MODO = "db"; //db per bdd
    
        // getAll devuelve la lista completa
        public static List<Llamada> getAll() {
            if (MODO.equals("db")){
                return DBDatosLlamada.getLlamadas();
            }
            return DBDatosLlamada.getLlamadas();
        }
    
       
    
    
        // getId devuelve un registro
        public static Llamada getLlamadaById(int id) {
        
            if (MODO.equals("db")){
                return DBDatosLlamada.getLlamadaId(id);
            }else{
                return DatosLlamada.getLlamadaId(id);
            }
            
        }
    
        // save guarda un Contacto
        // si es nuevo (id==0) lo añade a la lista
        // si ya existe, actualiza los cambios
        public static void save(Llamada llam) {
    
            if (MODO.equals("db")){
                if (llam.getId() > 0) {
                    DBDatosLlamada.updateLlamada(llam);
                } else {
                    DBDatosLlamada.newLlamada(llam);
                }
            }else{
                if (llam.getId() > 0) {
                    DatosLlamada.updateLlamada(llam);
                } else {
                    DatosLlamada.newLlamada(llam);
                }
            }
            
    
    
            
        }
    
        // size devuelve numero de LlamagetLlamadas
        public static int size() {
            
            if (MODO.equals("db")){
                return DBDatosLlamada.getLlamadas().size();
            }else{
                return DatosLlamada.getLlamadas().size();
            }
        }
    
        // removeId elimina Contacto por id
        public static void removeId(int id) {
            
            if (MODO.equals("db")){
                DBDatosLlamada.deleteLlamadaId(id);
            }else{
                DatosLlamada.deleteLlamadaId(id);
            }
        }
    
    
    }



    // // getAll devuelve la lista completa
    // public static List<Llamada> getAll(){
    //     return DatosLlamada.getLlamadas();
    // }

    // //getId devuelve un registro
    // public static Llamada getId(int id){
    //     return DatosLlamada.getLlamadaId(id);
    // }
   
    // //save guarda un Llamada
    // // si es nuevo (id==0) lo añade a la lista
    // // si ya existe, actualiza los cambios
    // public static void save(Llamada llam) {
    //     System.out.println("Llamando");
    //     if (llam.getId()>0){
    //         DatosLlamada.updateLlamada(llam);
    //     } else {
    //         DatosLlamada.newLlamada(llam);
    //     }
        
    // }

    // // size devuelve numero de Llamadas
    // public static int size() {
    //     return DatosLlamada.getLlamadas().size();
    // }


    // // removeId elimina Llamada por id
    // public static void removeId(int id){
    //     DatosLlamada.deleteLlamadaId(id);
    // }

