package com.enfocat.mvc;

import java.util.ArrayList;
import java.util.List;

public class Datos {
    
    private static List<Contacto> contactos = new ArrayList<Contacto>();
    private static int ultimoContacto = 0;

// Tenemos una array List en memoria, que empieza ya con dos contactos escritos. 

    static {
        contactos.add(new Contacto(1,"ana", "ana@gmail.com", "Barcelona", "935467225"));
        contactos.add(new Contacto(2,"joan", "joan@gmail.com", "Madrid", "914467894"));
        ultimoContacto=2;
    }

// Al último contacto le suma una unidad. Al contacto que se le pasa, le añade el número de id último contacto
//  y lo añade a la ArrayList y lo retorna. 

    public static Contacto newContacto(Contacto cn){
        ultimoContacto++;
        cn.setId(ultimoContacto);
        contactos.add(cn);
        return cn;
    }

// Para cada contacto cn, si este id es igual que el del contacto que me han dado, lo devuelvo. Si no coincidiera 
// el número, nos devuelve null. 
    public static Contacto getContactoId(int id){
        for (Contacto cn : contactos){
            if (cn.getId()==id){
                return cn;
            }
        }
        return null;
    }

// Si el id es el mismo que el anterior, busca el id de ese contacto,
// lo machaca y guarda encima el nuevo contacto con el set. Después cambia el valor de updated a true.

    public static boolean updateContacto(Contacto cn){
        boolean updated = false;
        for (Contacto x : contactos){
            if (cn.getId()==x.getId()){
                //x = cn;
                int idx = contactos.indexOf(x);
                contactos.set(idx,cn);
                updated=true;
            }
        }
        return updated;
    }


    public static boolean deleteContactoId(int id){
        boolean deleted = false;
        
        for (Contacto x : contactos){
            if (x.getId()==id){
                contactos.remove(x);
                deleted=true;
                break;
            }
        }
        return deleted;
    }

    public static List<Contacto> getContactos() {
        return contactos;
    }

    public static void setContactos(List<Contacto> contactos) {
        Datos.contactos = contactos;
    }


}

