package com.enfocat.mvc;

import java.util.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;

public class Llamada {
    int id;
    Date fecha;
    String notas;
    int idContacto;

    public Llamada(int id, String sfecha, String notas, int idContacto) {
        this.id = id;

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        try {
            this.fecha = sdf.parse(sfecha);
        } catch (ParseException e) {
            this.fecha = new Date();
        }

        this.notas = notas;
        this.idContacto = idContacto;
    }

    public Llamada(String sfecha, String notas, int idContacto) {
        
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        try {
            this.fecha = sdf.parse(sfecha);
        } catch (ParseException e) {
            this.fecha = new Date();
        }

        this.notas = notas;
        this.idContacto = idContacto;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }


    public String getFecha() {
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        return  sdf.format(this.fecha);
    }

    public String getFechaForm() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return  sdf.format(this.fecha);
    }


    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public String getNotas() {
        return notas;
    }

    public void setNotas(String notas) {
        this.notas = notas;
    }

    public int getIdContacto() {
        return idContacto;
    }

    public void setIdContacto(int idContacto) {
        this.idContacto = idContacto;
    }

}