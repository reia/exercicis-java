package com.enfocat.mvc;

import java.util.ArrayList;
import java.util.List;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;


import com.mysql.jdbc.Connection;



public class DBDatosLlamada {
    
    private static final String TABLE = "llamadas";
    private static final String KEY = "id"; 
   
    public static Llamada newLlamada(Llamada llam){
            String sql; 
            
                sql = String.format("INSERT INTO %s (fecha, notas, contactos_id) VALUES (?,?,?)", TABLE);
           
            try (Connection conn = DBConn.getConn();
                    PreparedStatement pstmt = conn.prepareStatement(sql);
                    Statement stmt = conn.createStatement()) {
                
                pstmt.setString(1, llam.getFechaForm());
                pstmt.setString(2, llam.getNotas());
                pstmt.setInt(3, llam.getIdContacto());
                pstmt.executeUpdate(); 
            
                //usuario nuevo, actualizamos el ID con el recién insertado
                ResultSet rs = stmt.executeQuery("select last_insert_id()"); 
                if (rs.next()) { 
                    llam.setId(rs.getInt(1));
                }
            
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }

            return llam;
        
    }


  
    public static Llamada getLlamadaId(int id){
        Llamada llam = null;
        String sql = String.format("select %s, fecha, notas, contactos_id from %s where %s=%d", KEY, TABLE, KEY, id);
        try (Connection conn = DBConn.getConn();
                Statement stmt = conn.createStatement()) {
            ResultSet rs = stmt.executeQuery(sql);
            if (rs.next()) {
                llam = new Llamada(
                    rs.getInt("id"),
                    rs.getString("fecha"),
                    rs.getString("notas"),
                    rs.getInt("contactos_id")
                    );
        
            }
        } catch (Exception e) {
            String s = e.getMessage();
            System.out.println(s);
        }
        return llam;
    }


    




    public static boolean updateLlamada(Llamada llam){
        boolean updated = false;
        String sql; 

       
            sql = String.format("UPDATE %s set fecha=?, notas=?, contactos_id=? where %s=%d", TABLE, KEY, llam.getId());
    
        try (Connection conn = DBConn.getConn();
                PreparedStatement pstmt = conn.prepareStatement(sql);
                Statement stmt = conn.createStatement()) {
            
            pstmt.setString(1, llam.getFechaForm());
            pstmt.setString(2, llam.getNotas());
            pstmt.setInt(3, llam.getIdContacto());
            pstmt.executeUpdate(); 
            updated = true;
        
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        return updated;
    }


    public static boolean deleteLlamadaId(int id){
        boolean deleted = false;
        String sql = String.format("DELETE FROM %s where %s=%d", TABLE, KEY, id);
        try (Connection conn = DBConn.getConn();
                Statement stmt = conn.createStatement()) {
            stmt.executeUpdate(sql);
            deleted=true;
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return deleted;
    }

    


    public static List<Llamada> getLlamadas() {

        List<Llamada> listaLlamadas = new ArrayList<Llamada>();  

        String sql = String.format("select id, fecha, notas, contactos_id from llamadas"); 
            
        try (Connection conn = DBConn.getConn();
        Statement stmt = conn.createStatement()) { 
    
        ResultSet rs = stmt.executeQuery(sql);  
        while (rs.next()) {
            Llamada u = new Llamada(
                rs.getInt("id"),
                rs.getString("fecha"),
                rs.getString("notas"),
                rs.getInt("contactos_id")
                );
            listaLlamadas.add(u);
            }
    
        } catch (Exception e) { 
        String s = e.getMessage();
        System.out.println(s);
        }
        return listaLlamadas; 
    }

    public static List<Llamada> getLlamadasContacto(int contactoId) {

        List<Llamada> listaLlamadas = new ArrayList<Llamada>();  

        String sql = String.format("select id, fecha, notas, contactos_id from llamadas where contactos_id="+contactoId); 
            
        try (Connection conn = DBConn.getConn();
        Statement stmt = conn.createStatement()) { 
    
        ResultSet rs = stmt.executeQuery(sql);  
        while (rs.next()) {
            Llamada u = new Llamada(
                rs.getInt("id"),
                rs.getString("fecha"),
                rs.getString("notas"),
                rs.getInt("contactos_id")
                );
            listaLlamadas.add(u);
            }
    
        } catch (Exception e) { 
        String s = e.getMessage();
        System.out.println(s);
        }
        return listaLlamadas; 
    }


}

