package com.enfocat.mvc;

import java.util.ArrayList;
import java.util.List;

public class DatosLlamada {
    
    private static List<Llamada> Llamadas = new ArrayList<Llamada>();
    private static int ultimaLlamada = 0;

// Tenemos una array List en memoria, que empieza ya con dos Llamadas escritos. 

    static {
        Llamadas.add(new Llamada(1, "29/01/2019", "Blablabla", 1));
        Llamadas.add(new Llamada(2, "40/03/2019", "Lalala", 2));
        ultimaLlamada=2;
    }

// Al último Llamada le suma una unidad. Al Llamada que se le pasa, le añade el número de id último Llamada
//  y lo añade a la ArrayList y lo retorna. 

    public static Llamada newLlamada(Llamada llam){
        System.out.println("id");
        ultimaLlamada++;
        llam.setId(ultimaLlamada);
        Llamadas.add(llam);
        System.out.println("id: " + llam.getId());
        return llam;
    }

// Para cada Llamada llam, si este id es igual que el del Llamada que me han dado, lo devuelvo. Si no coincidiera 
// el número, nos devuelve null. 
    public static Llamada getLlamadaId(int id){
        for (Llamada llam : Llamadas){
            if (llam.getId()==id){
                return llam;
            }
        }
        return null;
    }

// Si el id es el mismo que el anterior, busca el id de ese Llamada,
// lo machaca y guarda encima el nuevo Llamada con el set. Después cambia el valor de updated a true.

    public static boolean updateLlamada(Llamada llam){
        boolean updated = false;
        for (Llamada x : Llamadas){
            if (llam.getId()==x.getId()){
                //x = llam;
                int idx = Llamadas.indexOf(x);
                Llamadas.set(idx,llam);
                updated=true;
            }
        }
        return updated;
    }


    public static boolean deleteLlamadaId(int id){
        boolean deleted = false;
        
        for (Llamada x : Llamadas){
            if (x.getId()==id){
                Llamadas.remove(x);
                deleted=true;
                break;
            }
        }
        return deleted;
    }

    public static List<Llamada> getLlamadas() {
        return Llamadas;
    }


}

