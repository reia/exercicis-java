
package com.enfocat.buscaminas;

import java.util.Random;

public class Buscaminas {

    // Da el tamaño al campo de minas y el número.

    private static final int ANCHO = 20;
    private static final int ALTO = 10;
    private static final int MINAS = 40;
    private static boolean gameOver = false;

    // Array que contiene las distintas posiciones, que contiene filas y columnas.
    // Todos los elementos de la array
    // son objetos del tipo mina. Se generan con el método genera campo.

    private static Mina[][] campo = generaCampo(MINAS);
    public int status = 0;

    private static Mina[][] generaCampo(int minas) {

        // el Alto es el núm. de filas, el ancho el de columnas. En el for se crea una
        // Mina en cada lugar creado.
        // en este momento TODAS son verdes, ya que les pasamos false a todas.

        Mina[][] celdas = new Mina[ALTO][ANCHO];

        Random rnd = new Random();
        for (int y = 0; y < ALTO; y++) {
            for (int x = 0; x < ANCHO; x++) {
                Mina m = new Mina(false, x, y, false, 0, false);
                celdas[y][x] = m;
            }
        }

        // for del 0 hasta el número de minas. De manera random sobreescribe 5 objetos
        // verdes por minas,
        // cambiando la variable de Mina del objeto mina a true. Devuelve la array
        // celdas ya con las 5 bombas colocadas.

        for (int m = 0; m < minas; m++) {
            int rndX = rnd.nextInt(ANCHO);
            int rndY = rnd.nextInt(ALTO);
           Mina x= celdas[rndY][rndX];

           if (x.isMina()== false) {
            x.setMina(true);
            x.setWasBomb(true); } else {
                m--;
            } 
        }

        return celdas;
    }

    public static Mina[][] getCampo() {
        return Buscaminas.campo;
    }

    public static void reset() {
        Buscaminas.campo = generaCampo(MINAS);
    }

    public static void mostrar() {
        for (int y = 0; y < ALTO; y++) {
            for (int x = 0; x < ANCHO; x++) {
                Buscaminas.campo[y][x].setVisible(true);

            }
        }

    }

    public static int buscarMinas(int a, int b) {
        if (a < 0 || b < 0 || a > ANCHO - 1 || b > ALTO - 1) {
            return 0;
        }

        if (Buscaminas.campo[b][a].isMina() == true) {
            return 1;
        }

        return 0;
    }

    public static int minasCercanas(int x, int y) {

        int numeroMinas =     buscarMinas(x - 1, y - 1) 
                            + buscarMinas(x - 1, y + 1) 
                            + buscarMinas(x - 1, y)
                            + buscarMinas(x, y - 1) 
                            + buscarMinas(x, y + 1) 
                            + buscarMinas(x + 1, y - 1) 
                            + buscarMinas(x + 1, y)
                            + buscarMinas(x + 1, y + 1);

        return numeroMinas;
    }


    public static void revisaMinas(int x, int y) {

        revisar(x - 1, y - 1);
        revisar(x - 1, y + 1);
        revisar(x - 1, y);
        revisar(x, y - 1); 
        revisar(x, y + 1); 
        revisar(x + 1, y - 1);
        revisar(x + 1, y);
        revisar(x + 1, y + 1);
        
    }


    public static void revisar(int x, int y) {
        

            if (x<0 || y<0 || x>ANCHO-1 || y> ALTO-1) {
                        return;
                    }
        Mina mina = Buscaminas.campo[y][x];
        mina.setBombasCercanas(Buscaminas.minasCercanas(x, y));

            if(mina.isMina() == true || mina.isVisible() == true) {
                        return;
                    }
            if(mina.getBombasCercanas()> 0) {
                        mina.setVisible(true);
                }
            if(mina.getBombasCercanas()== 0) {
                    Buscaminas.clicar(x,y);
                }
           
        }

    public static void bandera(int x, int y) {
       if (Buscaminas.campo[y][x].isHasFlag()== false) {
        Buscaminas.campo[y][x].setMina(false);
        Buscaminas.campo[y][x].setVisible(true);
        Buscaminas.campo[y][x].setHasFlag(true);
    } else if (Buscaminas.campo[y][x].isHasFlag()== true) {
            Buscaminas.campo[y][x].setVisible(false);
            Buscaminas.campo[y][x].setHasFlag(false);
            if (Buscaminas.campo[y][x].isWasBomb() == true) {
                Buscaminas.campo[y][x].setMina(true);
            }
        }


    }

    public static void clicar(int x, int y) {

        Mina m = Buscaminas.campo[y][x];
        m.setVisible(true);
        m.setBombasCercanas(Buscaminas.minasCercanas(x, y));
        
        if (Buscaminas.campo[y][x].isMina() == true) {
            Buscaminas.setGameOver(true);
            Buscaminas.mostrar();
        }

        Buscaminas.revisaMinas(x,y);

    }

    public static String finalPartida(boolean gameOver) {
        String game = "";
        if (gameOver == true) {
            game = "<h1 class=\"gameOver\">Game Over!</h1>";
        }

        return String.format(game);
    }

    public static String htmlCampo() {

        // Se inicializa un StringBuilder (para crear Strings a base de muchos
        // fragmentos).
        // Se definen iniDivFila y finDiv porque se usan mucho.
        // Hace un doble bucle y se pide a cada mina que devuelva el html. (Es una
        // funcion que tiene el objeto)
        // para hacer que se haga en filas,
        // cuando queremos que salga una se añade iniDivFila en el bucle. Así queda en
        // forma de cuadro.

        StringBuilder sb = new StringBuilder();
        String iniDivFila = "<div class='fila'>";
        String finDiv = "</div>";

        sb.append("<div class='campo'>");
        for (int y = 0; y < ALTO; y++) {
            sb.append(iniDivFila);
            for (int x = 0; x < ANCHO; x++) {
                sb.append(Buscaminas.campo[y][x].html());
            }
            sb.append(finDiv);
        }
        sb.append(finDiv);

        return sb.toString();

    }

    public static boolean isGameOver() {
        return gameOver;
    }

    public static void setGameOver(boolean gameOver) {
        Buscaminas.gameOver = gameOver;
    }

}