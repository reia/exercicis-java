package com.enfocat.buscaminas;

public class Mina {
    
    private boolean visible;
    private boolean hasFlag;
    private boolean wasBomb;
    private int x;
    private int y;
    private boolean mina = false;
    private int bombasCercanas;

    public Mina(boolean mina, int x, int y, boolean hasFlag, int bombasCercanas, boolean wasBomb ){
        this.mina = mina;
        this.x=x;
        this.y=y;
        this.visible= false;
        this.hasFlag= false;
        this.bombasCercanas= 0;
        this.wasBomb = true;
    }

    

    public String html(){
        // Si es una mina, su clase es celda mina, si no es mina, su clase es solo celda. Es solamente para CSS.
        String css = (this.mina) ? "celda mina " : "celda";
        // Lo mismo si es visible o no visible.
        if(!this.visible) css+=" oculta";
        if(this.hasFlag) {css+= " flag";}
        // Dentro de cada mina se guarda data x y data y.
        if( this.bombasCercanas == 0 || this.visible== false || this.hasFlag== true) {
        return String.format("<div class='%s' data-x='%d' data-y='%d'></div>", css, this.x, this.y);
        } else {
            return String.format("<div class='%s' data-x='%d' data-y='%d'>%d</div>", css, this.x, this.y,this.bombasCercanas);
        }
    }




    public boolean isVisible() {
        return visible;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public boolean isMina() {
        return mina;
    }

    public void setMina(boolean mina) {
        this.mina = mina;
    }

    public boolean isHasFlag() {
        return hasFlag;
    }

    public void setHasFlag(boolean hasFlag) {
        this.hasFlag = hasFlag;
    }

    public int getBombasCercanas() {
        return bombasCercanas;
    }

    public void setBombasCercanas(int bombasCercanas) {
        this.bombasCercanas = bombasCercanas;
    }

    public boolean isWasBomb() {
        return wasBomb;
    }

    public void setWasBomb(boolean wasBomb) {
        this.wasBomb = wasBomb;
    }


}

