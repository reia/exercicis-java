<%@page contentType="text/html; charset=UTF-8" %>
<%@page import="com.enfocat.buscaminas.Buscaminas" %>


    
<%

    if (request.getParameter("reset")!=null){
        Buscaminas.setGameOver(false);
        Buscaminas.reset(); 
    }

   if (request.getParameter("mostrar")!=null){
        Buscaminas.mostrar(); 
    }

    if (request.getParameter("zeta") == null) {

    } else if (request.getParameter("zeta").equals("right")) {

        if (request.getParameter("x")!=null){
        
        int x = Integer.parseInt(request.getParameter("x"));
        int y = Integer.parseInt(request.getParameter("y"));
        Buscaminas.bandera(x,y); 
    } 

    }else if (request.getParameter("zeta").equals("left")) {

    if (request.getParameter("x")!=null){
        
        int x = Integer.parseInt(request.getParameter("x"));
        int y = Integer.parseInt(request.getParameter("y"));
        Buscaminas.clicar(x,y); 
    }
    } else {

    }


%>
  

<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link href="https://fonts.googleapis.com/css?family=Monoton&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="css/estilos.css">
</head>
<body>

<h1>Buscaminas JSP</h1>


<%-- Crea el campo de minas. Devuelve un String, que es HTML --%>

<%= Buscaminas.htmlCampo() %>

<br>
<br>
<button id="reset">Reset</button>
<button id="mostrar">Mostrar</button>

<script src="https://code.jquery.com/jquery-3.3.1.min.js" crossorigin="anonymous"></script>
 
<script>

// Se guardan el data x y el data y de la celda que estamos clicando. Es muy útil para vincular eventos a elementos de la página.
// $(document).on("click", "div.celda.oculta", function(){
//     let url = window.location.href.split("?")[0];
//     let x=$(this).data("x");
//     let y=$(this).data("y");

// recarga la página pasándole esos dos parámetros. 
//     window.location.href = url + "?x="+x+"&y="+y;
// }

// Si se clica en el boton de reset, queremos volver a la URL vacia (ya habremos pulsado algunas bombas y habrán coordenadas después).
// La url actual se guarda en window.location.href. Lo que hacemos es guardarla y cortarla (split). Crea una array que separa lo de
// antes del interrogante de lo de después. Nos quedamos la 1a parte que es la que está en la posición [0]. Después se le añade
// la parte del reset, para que lo coja desde 0.

$(document).on("click", "button#reset", function(){
    let url = window.location.href.split("?")[0];
    window.location.href = url + "?reset=1";
})

$(document).on("click", "button#mostrar", function(){
    let url = window.location.href.split("?")[0];
    window.location.href = url + "?mostrar=1";
})

$(document).on("click", "div.celda.oculta", function(){
    let url = window.location.href.split("?")[0];
            let x=$(this).data("x");
            let y=$(this).data("y");
            let zeta= 'left';
            window.location.href = url +"?zeta="+zeta+"&x="+x+"&y="+y;
});

   $(document).on("contextmenu", "div.celda.oculta, div.celda.flag", function(e) {
    
         if (e.button == 2) {
             e.preventDefault();
            let url = window.location.href.split("?")[0];
            let x=$(this).data("x");
            let y=$(this).data("y");
            let zeta= 'right';
            window.location.href = url + "?zeta="+zeta+"&x="+x+"&y="+y;

    }

   });
    


</script>

<%= Buscaminas.finalPartida(Buscaminas.isGameOver()) %>
</body>
</html>